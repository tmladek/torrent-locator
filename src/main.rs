use clap::{App, Arg};
use lava_torrent::torrent::v1::Torrent;
use log::{debug, error, info};
use std::path::PathBuf;
use walkdir::WalkDir;

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .init();

    let matches = App::new("Torrent Locator")
        .arg(
            Arg::with_name("SEARCHPATH")
                .help("Filesystem path to walk through")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("TORRENTFILES")
                .help("Torrent files to parse")
                .multiple(true)
                .required(true),
        )
        .get_matches();

    let search_path = matches.value_of("SEARCHPATH").unwrap();
    let torrent_files = matches
        .values_of("TORRENTFILES")
        .unwrap()
        .collect::<Vec<_>>();

    info!(
        "Searching through \"{}\" and checking {} torrent file(s)...",
        search_path,
        torrent_files.len()
    );
    debug!("Torrent files: {}", torrent_files.join(", "));

    let torrents: Vec<(&str, Torrent)> = torrent_files
        .into_iter()
        .map(|path| (path, Torrent::read_from_file(path)))
        .filter_map(|(path, torrent)| match torrent {
            Ok(torrent) => Some((path, torrent)),
            Err(err) => {
                error!("Error loading \"{path}\"! {err}");
                None
            }
        })
        .collect();

    for dir_entry in WalkDir::new(search_path).into_iter() {
        if let Ok(dir_entry) = dir_entry {
            let path = dir_entry.into_path();
            let last = path
                .components()
                .last()
                .unwrap()
                .as_os_str()
                .to_str()
                .unwrap_or_else(|| panic!("Could not convert \"{:?}\" to utf-8.", path));

            for (torrent_file, torrent) in &torrents {
                if torrent.name == last {
                    let dir_path = path
                        .as_path()
                        .components()
                        .collect::<Vec<_>>()
                        .split_last()
                        .unwrap()
                        .1
                        .iter()
                        .collect::<PathBuf>();

                    println!("{}\t{}\t{}", torrent_file, torrent.name, dir_path.display());
                }
            }
        } else {
            error!("{}", dir_entry.err().unwrap());
        }
    }
}
